'''
Created on 18 sep. 2020

@author: RSSpe

1) Crear (instanciacion).......... [HECHO]
2) Llenar (inicializacion).......... [HECHO]
3) obtener inicio.......... [HECHO]
4) obtener fin.......... [HECHO]
5) obtener cantidad de elementos.......... [HECHO]
6) Mostrar todos los elementos.......... [HECHO]
7) Mostrar elemento del inicio.......... [HECHO]
8) Mostrar elemento del fin.......... [HECHO]
9) Disminuir tamaño del arreglo.......... [HECHO]
10) Insertar elemento en posicion especifica.......... [HECHO]
11) Eliminar elemento de posicion especifica.......... [HECHO]
12) Invertir el vector.......... [HECHO]
'''

class VectorEspecial:
    "" "Clase para crear un array personalizado" ""
    def __init__(self):
        self.__edades = []
        self.__edad = 0


    def llenar(self) -> None:
        "" "Metodo para llenar el vector" ""
        while(True):

            while(True):
                self.__edad = int(input("Introduce edad: "))
    
                if(self.__edad<=0):
                    print("No puedes ingresar esa edad, por favor vuelve a intentarlo")
                else:
                    self.__edades.append(self.__edad)
                    break
            
            while(True):
                print("¿Que quieres hacer?\n1.- Agregar edad\n2.- Dejar de agregar edad\n")
                self.__opcion = int(input("Introduce opcion: "))
                
                if(not self.__opcion == 1 and not self.__opcion == 2):
                    print("No existe esa opcion, por favor prueba de nuevo")
                else:
                    break

            if(self.__opcion==2):
                break


    def obtenerInicio(self) -> int:
        "" "Metodo para obtener el inicio del array" ""
        if len(self.__edades)!=0:
            return len(self.__edades)-1
        return "No hay elementos en el vector"


    def obtenerFin(self) -> int:
        "" "Metodo para obtener el fin del array" ""

        if len(self.__edades)!=0:
            return len(self.__edades)-1
        return "No hay elementos en el vector"


    def obtenerCantidadDeElementos(self) -> int:
        "" "Metodo para obtener la cantidad de elementos del array" ""
        return len(self.__edades)
    
    
    def mostrarTodosLosElementos(self) -> None:
        "" "Metodo para mostrar el contenido del array" ""

        if len(self.__edades)==0:
            print("No hay elementos en el vector")
        else:
            print(self.__edades,"\n")


    def mostrarElementoInicio(self) -> None:
        "" "Metodo para mostrar el primer elemento del array" ""

        if len(self.__edades)==0:
            print("No hay elementos en el vector")
        else:
            print(self.__edades[0],"\n")


    def mostrarElementoFin(self) -> None:
        "" "Metodo para mostrar el ultimo elemento del array" ""
        if len(self.__edades)==0:
            print("No hay elementos en el vector")
        else:
            print(self.__edades[len(self.__edades)-1],"\n")


    def disminuirTamañoDelArreglo(self) -> None:
        "" "Metodo para disminuir el tamaño del array" ""
        
        if(len(self.__edades)!=0):
            while True:
                decremento = int(input("Ingresa la cantidad de espacios que se reduciran: "))
            
                if decremento>len(self.__edades):
                    print("No se puede eliminar mas espacios de los que ya tiene el array\n")
                elif decremento==0:
                    print("Eliminar 0 espacios es innecesario, prueba con otra cantidad\n")
                else:
                    for i in range(decremento):
                        self.__edades.pop()
                    break
        else:
            print("El array esta vacio\n")
    
    
    def insertarElementoEnPosicion(self) -> None:
        "" "Metodo para insertar un elemento en una posicion especifica" ""
        while(True):
            self.__posicion = int(input("Introduce posicion: "))

            if self.__posicion>=0 and self.__posicion<len(self.__edades):
                break
            else:
                print("No existe esa posicion, por favor vuelve a intentarlo")

        while(True):
            self.__edad = int(input("Introduce edad: "))

            if(self.__edad <=0):
                print("No puedes ingresar esa edad, por favor vuelve a intentarlo\n")
            else:
                break
        
        self.__edades[self.__posicion] = self.__edad


    def eliminarElementoEnPosicion(self) -> None:
        "" "Metodo para eliminar un elemento en una posicion especifica" ""
        if(len(self.__edades)!=0):
            while(True):
                self.__posicion = int(input("Introduce posicion: "))

                if self.__posicion>=0 and self.__posicion<len(self.__edades):
                    self.__edades[self.__posicion] = 0
                    break
                else:
                    print("No puedes ingresar esa posicion, por favor vuelve a intentarlo\n")
        else:
            print("El array esta vacio\n")


    def invertirVector(self) -> None:
        "" "Metodo para invertir nuestro array" ""
        if (len(self.__edades)>=2):
            self.__edadesCopia = self.__edades.copy()

            for i in range(len(self.__edadesCopia)):
                self.__edades[i] = self.__edadesCopia[len(self.__edadesCopia)-1-i]

            print("Array invertido con exito\n")
        else:
            print("No notaras la diferencia a menos que haya 2 o\nmas valores por favor, prueba cuando tengas mas valores\n")
