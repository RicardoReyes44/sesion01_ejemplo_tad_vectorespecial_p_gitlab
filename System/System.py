'''
Created on 18 sep. 2020

@author: RSSpe
'''

from .VectorEspecial import VectorEspecial

class System:
    "" "Clase para gestionar el sistema" ""
    def __init__(self):
        self.__candado = True
        self.__ve = VectorEspecial()


    def ejecutar(self) -> None:
        "" "Funcion para comenzar el programa" ""
        while self.__candado:

            print("1) Crear\n"
            + "2) Llenar\n"
            + "3) Obtener inicio\n"
            + "4) Obtener fin\n"
            + "5) Obtener cantidad de elementos\n"
            + "6) Mostrar todos los elementos\n"
            + "7) Mostrar elemento del inicio\n"
            + "8) Mostrar elemento del fin\n"
            + "9) Disminuir tamaño del arreglo\n"
            + "10) Insertar elemento en posicion especifica\n"
            + "11) Eliminar elemento de posicion especifica\n"
            + "12) Invertir el vector\n"
            + "13) Salir\n")
            
            try:
                self.__opcion = int(input("Introduce una opcion: "))
                
                if(self.__opcion==1):
                    self.__ve = VectorEspecial()

                elif(self.__opcion==2):
                    self.__ve.llenar()

                elif(self.__opcion==3):
                    print(self.__ve.obtenerInicio(),"\n")

                elif(self.__opcion==4):
                    print(self.__ve.obtenerFin(),"\n")

                elif(self.__opcion==5):
                    print(self.__ve.obtenerCantidadDeElementos(),"\n")

                elif(self.__opcion==6):
                    self.__ve.mostrarTodosLosElementos()

                elif(self.__opcion==7):
                    self.__ve.mostrarElementoInicio()

                elif(self.__opcion==8):
                    self.__ve.mostrarElementoFin()

                elif(self.__opcion==9):
                    self.__ve.disminuirTamañoDelArreglo()

                elif(self.__opcion==10):
                    self.__ve.insertarElementoEnPosicion()

                elif(self.__opcion==11):
                    self.__ve.eliminarElementoEnPosicion()

                elif(self.__opcion==12):
                    self.__ve.invertirVector()

                elif(self.__opcion==13):
                    self.__detener()

                else:
                    print("Esa opcion no existe, por favor vuelve a intentarlo")

            except ValueError as e:
                print("Entrada invalida <", e, ">\n\n--------------------------\nMenu principal\n--------------------------\n")


    def __detener(self) -> None:
        "" "Funcion para terminar el programa" ""
        self.__candado = False
