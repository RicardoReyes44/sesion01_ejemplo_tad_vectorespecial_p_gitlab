'''
Created on 18 sep. 2020

@author: RSSpe
'''

from System.System import System


def main() -> None:
    "" "Ejecutar el programa" ""
    system = System()
    system.ejecutar()

if __name__ == '__main__':
    main()
    print("\n------------------------------------\nPrograma terminado\n------------------------------------\n")
    